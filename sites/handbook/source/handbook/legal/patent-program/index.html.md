---
layout: handbook-page-toc
title: "GitLab Patent Program"
description: "Learn about GitLab's Patent Program"
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Innovation at GitLab

Innovation is key to GitLab’s success. The GitLab Patent Program seeks to maximize the competitive and commercial value of GitLab’s intellectual property rights and capitalize on the time and effort invested by team members in developing patentable inventions. The Program establishes a process to systematically identify valuable inventions, assess their patentability and, where appropriate, enable successful prosecution and registration of a patent covering the invention.


## How to participate

To disclose an invention, complete an [Invention Disclosure Form](https://docs.google.com/document/d/1BndxggJyA3aMdYav-ghNwpTdNcfgJcrHFRlT6u8KFD8/copy) and email it to [innovation-program@gitlab.com](mailto:innovation-program@gitlab.com).

You should submit an Invention Disclosure Form any time you think you may have made an invention. **The sooner you submit the Form, the better.** Since you may be overly conservative in determining what constitutes an invention, you should submit a Form even if you aren’t sure that your work is sufficiently novel to be an invention. So, err on the side of disclosing, leaving it to the Legal Team, and our external patent specialists, to decide whether to file, retain as a trade secret, or publish to preempt.

In order to maintain the confidentiality of your invention, do not discuss it on a public or even confidential GitLab issue or on any slack channels - all correspondence concerning the invention should take place via [innovation-program@gitlab.com](mailto:innovation-program@gitlab.com).


## Conditions of participation

Inventors must be current GitLab Team Members in order to participate in the GitLab Patent Program. GitLab may change or terminate the Program at any time. GitLab reserves the right in its sole discretion to decide whether to file for a patent on submissions or retain submissions as a trade secret or publish to preempt. Participation in the GitLab Patent Program is subject to local employment and intellectual property laws.
