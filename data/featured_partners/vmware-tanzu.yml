title: GitLab on VMware Tanzu
canonical_path: /partners/technology-partners/vmware-tanzu/
seo_title: GitLab on VMware Tanzu
description: Empower your team to continuously deliver better code with an open DevOps platform to plan, build, test, and deploy on VMware Tanzu.
title_description: Empower your team to continuously deliver better code with an open DevOps platform to plan, build, test, and deploy on VMware Tanzu.
logo: /images/logos/vmware_tanzu_vertical.png
logo_alt: VMware Tanzu Logo
body_top_title: Run anywhere together
body_top_description: "Develop cloud native applications for the VMware Tanzu platform. Free workloads from infrastructure to work independently and run anywhere with GitLab – a single solution for everyone on your pipeline. Build, secure, deploy, and migrate applications to VMware services via a complete DevSecOps self-service model with GitLab and Tanzu. The finish line: digital transformation. GitLab gets you there."
body_top_cta_1_text: Read the blog
body_top_cta_2_text: Start your free trial
body_top_cta_img_1: "/images/topics/featured-partner-vmware-tanzu-1.png"
body_top_cta_img_2: "/images/topics/featured-partner-vmware-tanzu-2.svg"
body_top_cta_1_url: "https://tanzu.vmware.com/developer/guides/ci-cd/gitlab-ci-cd-cnb/"
body_top_cta_2_url: /free-trial/
body_features_title: Develop better cloud native applications faster with GitLab and VMware Tanzu
body_features_description: Reduce friction across teams and workflows to compress cycle times with GitLab’s consistent, scalable interface. Iterate faster and innovate together with built-in planning, monitoring, and reporting solutions plus tight VMware Tanzu integration.
body_quote: "Enabling automated deployments with verified Bitnami images and codified policies enables a Continuous Verification process which can reduce costs, security risks, and potential performance issues. We’re excited to work with partners such as GitLab to empower customers to fully leverage their cloud investments."
body_quote_source: Milin Desai, GM, Cloud Services at VMware
feature_items:
  - title: "One-for-all collaboration"
    description: "Optimize contributions. From issue tracking to code review, GitLab reduces rework. Happier developers expand product roadmaps instead of repairing old roads."
  - title: "Unshakable automation"
    description: "Deploy unstoppable software with DevSecOps. Automated workflows increase uptime by reducing security and compliance risks on VMware Tanzu."
  - title: "Countless wins"
    description: "Mature, profit, repeat. Grow your market share and revenue when you continuously deliver on-budget, on-time, and always-up products."
body_middle_title: "Get started with GitLab and VMware Tanzu Joint Solutions"
body_middle_description: 'GitLab is a featured DevOps solution in the <a href="https://tanzu.vmware.com/solutions-hub/devops-tooling/gitlab">VMware Tanzu Marketplace</a>. As an open core platform, GitLab integrates with your current processes so you can adopt an end-to-end software delivery lifecycle while maintaining the investment in your current toolchain. Leverage GitLab and VMware Tanzu joint solutions to create a continuous integration (CI) pipeline with Kubernetes, enable Continuous Verification, and more.'
body_middle_items:
  - title: "VMware Tanzu Application Service (TAS)"
    description: "Leverage automated workflows with GitLab CI/CD to deploy ava, .NET, and Node applications on TAS. GitLab on TAS is a joint solution for cloud native development, legacy app migration, and modernization."
    icon: "/images/logos/TZLG-VMwareTanzu-icon.png"
    link: "https://tanzu.vmware.com/application-service"
  - title: "VMware Tanzu Kubernetes Grid (TKGI) validated"
    description: "Helm chart delivers GitLab to VMware-based Kubernetes clusters with VMware Tanzu Kubernetes Grid (TKGI). Visit GitLab through the webUI and interact with it via the standardized Git command-line application. GitLab runs on TKGI with no dependency on TAS."
    icon: "/images/features/kubernetes-logo.png"
    link: "https://tanzu.vmware.com/kubernetes-grid"
  - title: "VMware Tanzu Observability by Wavefront"
    description: "Get 3D observability with the Wavefront GitLab integration, featuring a GitLab dashboard for HTTP, process, and network stats. Fetch and push GitLab metrics to Wavefront by installing the Telegraf Agent and enabling the Prometheus Input Plugin."
    icon: "/images/logos/TZLG-VMwareTanzu-icon.png"
    link: "https://tanzu.vmware.com/observability"
  - title: "CloudHealth and GitLab"
    description: "Together, GitLab CI/CD and CloudHealth provide Day-2 operations for budget and resource management with easy access to insights via an API integration. Enable project-specific pipeline checks and governance rules to reduce the footprint of your deployments."
    icon: "/images/logos/cloud-health.png"
    link: "https://www.cloudhealthtech.com/"
  - title: "GitLab Integration in Cloud Assembly"
    description: "Harness GitLab CI/CD iterative workflows to develop Infrastructure as Code (IaC) and deploy to your public and private cloud providers with GitLab in Cloud Assembly. Enable blueprint authoring to directly commit changes to blueprints repositories stored in GitLab and synchronize into VMware Cloud Assembly projects with automated tasks."
    icon: "/images/logos/cloud-assembly.png"
    link: "https://www.vmware.com/products/vrealize-automation.html"
benefits_title: Discover the benefits of GitLab on VMware Tanzu
featured_video_title: "Opening Keynote: The Power of GitLab - Sid Sijbrandij"
featured_video_url: "https://www.youtube-nocookie.com/embed/xn_WP4K9dl8"
featured_video_topic: "GitLab Commit Virtual 2020"
featured_video_more_url: "https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg/featured"
resources:
  - title: GitLab Tanzu Services Marketplace Listing
    url: https://tanzu.vmware.com/solutions-hub/devops-tooling/gitlab
    type: Blog
  - title: VMworld-2020-demo/spring-music demo
    url: https://gitlab.com/gitlab-com/alliances/vmware/sandbox/vmworld-2020-demo/spring-music
    type: Blog
  - title: GitLab to Enable Cloud Native Transformation on VMware Cloud Marketplace
    url: https://www.globenewswire.com/news-release/2019/09/17/1916738/0/en/GitLab-to-Enable-Cloud-Native-Transformation-on-VMware-Cloud-Marketplace.html
    type: Blog
  - title: Webinar - Cloud Native
    url: https://tanzu.vmware.com/content/webinars/jul-30-best-practices-for-cloud-native-pipelines-with-gitlab-and-vmware-tanzu?utm_campaign=Global_BT_Q221_Best-Practices-Cloud-Native-Pipeline-Gitlab&utm_source=sales-email&utm_medium=partner
    type: Blog
cta_banner:
  - title: How can GitLab integrate with your VMware Tanzu infrastructure?
    button_url: https://about.gitlab.com/sales/
    button_text: Talk to an expert
